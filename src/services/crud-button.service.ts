import { GridService } from './grid.service';
import { Config } from './../environments/config';
import { UtilService } from './util.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CrudBtn } from '../models/crud-btn.model';
import { Router } from '@angular/router';
import { EditarService } from './editar.service';

@Injectable()
export class CrudButtonService {

  private _crudButtons: Array<CrudBtn> = [{
    titulo: 'INCLUIR',
    id: Config.IDSUBJECT.rotaIncluir,
    cor: Config.CLASSES_PADRONIZADAS.BtnIncluir,
    enable: true,
    visible: false,
    split: false,
    options: null
  }, {
    titulo: 'EDITAR',
    id: '20',
    cor: '#4F8AD8',
    enable: true,
    visible: false,
    split: true,
    options: [{
      label: 'Edição como Rota',
      id: '400',
      command: (event) => {
        if (this.gridSrv.selectedRows.length !== 1 || this.gridSrv.selectedRows.length === undefined) {
          console.log('N tem comp');
        } else {
/*           this.editarSrv.displayModal = true;
          this.editarSrv.displayRota = false; */
          const res = this.gridSrv.selectedRows[0];
          this.route.navigateByUrl('Editar');
          this.utilSrv.ChamarFuncao(res, Config.IDSUBJECT.rotaEditar);
        }
      }
    },
    {
      label: 'Edição como Modal',
      id: '400',
      command: (event) => {
        if (this.gridSrv.selectedRows.length !== 1 || this.gridSrv.selectedRows.length === undefined) {
          console.log('N tem comp');
        } else {
/*         this.editarSrv.displayModal = true;
        this.editarSrv.displayRota = false; */
        const res = this.gridSrv.selectedRows[0];
        this.utilSrv.ChamarFuncao(res, Config.IDSUBJECT.modalEditar);
        /* this.utilSrv.ChamarFuncao(null, Config.IDSUBJECT.modalEditar); */
      }
    }
    }]

  }, {
    titulo: 'EXCLUIR',
    id: '30',
    cor: '#D10429',
    enable: true,
    visible: false,
    split: false,
    options: null

  },
  {
    titulo: 'LISTAR',
    id: '40',
    cor: '#636266',
    enable: true,
    visible: false,
    split: false,
    options: null

  },
  {
    titulo: 'EXPORTAR',
    id: '50',
    cor: '#6D8046',
    enable: true,
    visible: false,
    split: false,
    options: null
  }
  ];

  constructor(
    public http: HttpClient
    , public route: Router
    , public utilSrv: UtilService
    , public gridSrv: GridService
    , public editarSrv: EditarService) { }

  /**
   * Ativa um botão
 * @param item
   */
  public enableBtn(item: CrudBtn) {

  }



  get buttonList() {
    return this._crudButtons;
  }


  /**
   *
   * Desativa um botão
   * @param {CrudBtn} item
   * @memberof CrudButtonService
   */
  public disable(item: CrudBtn) {

  }
  /**
   * Adiciona um novo Botão à lista
   *
   * @param {CrudBtn} item
   * @memberof CrudButtonService
   */
  public addButton() {

  }
  /**
   * Checa se o botão à ser adicionado ja existe na Lista de botões
   *
   * @param {CrudBtn} item
   * @returns {CrudBtn}
   * @memberof CrudButtonService
   */
  checkObjectById(item: CrudBtn): CrudBtn {
    let retorno = this._crudButtons.find(x => x.id === item.id);
    if (retorno === undefined) {
      retorno = this.empty();
    }
    return retorno;

  }

  public empty(): CrudBtn {
    return <CrudBtn>{ titulo: '', id: '', cor: '', enable: false, visible: true, split: false, options: null };
  }
  private pesquisarBotao(item: CrudBtn): CrudBtn {
    const obj: CrudBtn = {
      titulo: '', id: '', cor: '', enable: false, visible: true, split: false, options: [{ label: 'string' }]
    };
    return obj;
  }
}
