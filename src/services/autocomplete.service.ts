import { Injectable } from '@angular/core';
import { Subject, Observable, observable } from 'rxjs';
import { AutoComplete } from '../models/autocomplete.model';

@Injectable()
export class AutoCompleteService {
  private _AutoCompletes: Array<AutoComplete> = [];
  public AutoCompleteSelecionado = new Subject<AutoComplete>();

  constructor() { }

  get autocompleteList() {
    return this._AutoCompletes;
  }

  set autoCompleteList(dados) {
    this._AutoCompletes = dados;
  }


  getElementos(): Observable<any> {
    return Observable.create((observer: any) => {
      observer.next(this._AutoCompletes);
    });
  }

  /**
   * Ativa um botão
   * @param autocomplete
   */
  public enable(autocomplete: AutoComplete) {

  }

  /**
   *
   * Desativa um botão
   * @param {AutoComplete} autocomplete
   * @memberof AutoCompleteService
   */
  public disable(autocomplete: AutoComplete) {

  }
}
