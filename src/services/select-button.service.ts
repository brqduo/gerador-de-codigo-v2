import { Injectable } from '@angular/core';
import { SelectButton } from '../models/select-button.model';

@Injectable()
export class SelectButtonService {
  private _SelectButtons: Array<any> = [];

  constructor() { }

  get dropdownList() {
    return this._SelectButtons;
  }

  set dropdownList(dados) {
    this._SelectButtons = dados;
  }

  /**
   * Ativa um botão
   * @param selectButton
   */
  public enable(selectButton: SelectButton) {

  }

  /**
   *
   * Desativa um botão
   * @param {SelectButton} selectButton
   * @memberof SelectButtonService
   */
  public disable(selectButton: SelectButton) {

  }
}
