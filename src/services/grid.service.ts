import { Injectable, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';
import { Config } from '../environments/config';
import { GridOptions } from 'ag-grid-community';

@Injectable()
export class GridService {
  GridApi: GridOptions;
  private _gridSelecteds: Array<any> = [];
  constructor(public http: HttpClient, public dataSrv: DataService) { }


  set selectedRows(rows: Array<any>) {
    this._gridSelecteds = rows;
  }
  get selectedRows(): Array<any> {
    return this._gridSelecteds;
  }

}
