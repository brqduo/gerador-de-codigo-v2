import { Injectable } from '@angular/core';
import { Slider } from '../models/slider.model';

@Injectable()
export class SliderService {
  private _Sliders: Array<Slider> = [];

  constructor() { }

  get sliderList() {
    return this._Sliders;
  }


  set sliderList(dados) {
    this._Sliders = dados;
  }

  /**
   * Ativa um botão
   * @param slider
   */
  public enable(slider: Slider) {

  }

  /**
   *
   * Desativa um botão
   * @param {Slider} slider
   * @memberof SliderService
   */
  public disable(slider: Slider) {

  }
}
