import { Injectable } from '@angular/core';

@Injectable()
export class PesquisaService {

  private _camposPesquisa = [];
  private _title = '';
  private _idPesquisa= '';

  constructor() { }

  set camposPesquisa(campos: any) {
    this._camposPesquisa = campos;
  }

  set idPesquisa(id:any){
    this._idPesquisa = id;
  }

  get camposPesquisa() {
    return this._camposPesquisa;
  }

  get idPesquisa(){
    return this._idPesquisa;
  }


  /**
   * Insere o título na variável
   * @param titulo
   */
  set title(titulo) {
    this._title = titulo;
  }

  get title() {
    return this._title;
  }
}
