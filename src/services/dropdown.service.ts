import { Injectable } from '@angular/core';
import { Dropdown } from '../models/dropdown.model';

@Injectable()
export class DropdownService {
  private _Dropdowns: Array<Dropdown> = [];

  constructor() { }

  get dropdownList() {
    return this._Dropdowns;
  }

  set dropdownList(dados) {
    this._Dropdowns = dados;
  }

  /**
   * Ativa um botão
   * @param dropdown
   */
  public enable(dropdown: Dropdown) {

  }

  /**
   *
   * Desativa um botão
   * @param {Dropdown} dropdown
   * @memberof DropdownService
   */
  public disable(dropdown: Dropdown) {

  }
}
