import { Injectable } from '@angular/core';
import { Calendar } from '../models/calendar.model';
import { Subject } from 'rxjs';
import { data } from '../app/mock/data';

@Injectable()
export class CalendarService {
  private _Calendars: Array<Calendar> = [];

  constructor() { }

  get calendarList() {
    return this._Calendars;
  }

  set calendarList(dados) {
    this._Calendars = dados;
  }

  /**
   * Ativa um botão
   * @param calendar
   */
  public enable(calendar: Calendar) {

  }

  /**
   *
   * Desativa um botão
   * @param {Calendar} calendar
   * @memberof CalendarService
   */
  public disable(calendar: Calendar) {

  }
}
