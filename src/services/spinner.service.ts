import { Injectable } from '@angular/core';
import { Spinner } from '../models/spinner.model';
import { Subject } from 'rxjs';

@Injectable()
export class SpinnerService {
  private _Spinners: Array<Spinner> = [];

  constructor() { }

  get spinnerList() {
    return this._Spinners;
  }

  set spinnerList(dados) {
    this._Spinners = dados;
  }

  /**
   * Ativa um botão
   * @param spinner
   */
  public enable(spinner: Spinner) {

  }

  /**
   *
   * Desativa um botão
   * @param {Spinner} spinner
   * @memberof SpinnerService
   */
  public disable(spinner: Spinner) {

  }
}
