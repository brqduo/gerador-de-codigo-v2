import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../environments/config';
import { UtilService } from '../services/util.service';
import { DataSubjectModel } from '../models/dataSubject.model';
@Injectable()
export class DataService {

  constructor(public http: HttpClient,
    private UtilSrv: UtilService) {
    this.UtilSrv.FuncaoChamada.subscribe((data: DataSubjectModel) => {
    //  if (data.idSubject === Config.IDSUBJECT.pesquisa) {
        console.log(data);
      //}
    });
  }

  loadData() {
    this.http.get('https://my.api.mockaroo.com/users.json?key=a5e40240')
      .subscribe((res: any) => {

    });
  }

  /**
   * Lê o arquivo que contém o json para retornar
   * @param arquivo Nome do arquivo a ser lido
   */
  carregarArquivosJson(arquivo): Observable<any> {
    return this.http.get(this.UtilSrv.urlObjeto + arquivo)
      .pipe(map((data: any[]) => {
        return data;
      }));
  }

  carregarFiltros(destino): Observable<any> {
    return this.http.get(this.UtilSrv.urlColunas + destino)
      .pipe(map((data: any[]) => {
        return data;
      }));
  }


}
