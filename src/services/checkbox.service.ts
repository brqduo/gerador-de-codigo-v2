import { Injectable } from '@angular/core';
import { Checkbox } from '../models/checkbox.model';
import { Subject } from 'rxjs';

@Injectable()
export class CheckboxService {
  private _Checkboxs: Array<Checkbox> = [];
  public CheckboxSelecionado = new Subject<Checkbox>();

  constructor() { }

  get checkboxList() {
    return this._Checkboxs;
  }

  set checkboxList(dados) {
    this._Checkboxs = dados;
  }

  /**
   * Ativa um botão
   * @param checkbox
   */
  public enable(checkbox: Checkbox) {

  }

  /**
   *
   * Desativa um botão
   * @param {Checkbox} checkbox
   * @memberof CheckboxService
   */
  public disable(checkbox: Checkbox) {

  }
}
