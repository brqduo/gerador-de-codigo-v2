import { Injectable } from '@angular/core';
import { Mask } from '../models/mask.model';
import { Subject } from 'rxjs';

@Injectable()
export class MaskService {

  private _Masks: Array<Mask> = [];

  constructor() { }

  get maskList() {
    return this._Masks;
  }

  set maskList(dados) {
    this._Masks = dados;
  }

  /**
   * Ativa um botão
   * @param mask
   */
  public enable(mask: Mask) {

  }

  /**
   *
   * Desativa um botão
   * @param {Mask} mask
   * @memberof MasksService
   */
  public disable(mask: Mask) {

  }
}
