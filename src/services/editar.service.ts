import { Injectable } from '@angular/core';
import { EditarModel } from '../models/editar.model';

@Injectable({
  providedIn: 'root'
})
export class EditarService {
  private _displayModal: boolean;
  private _displayRota: boolean;

  private _objetosTela = {
    titulo: null as string,
    id: null as number,
    ordem: null as any,
    enable: false,
    visible: false,
    nome: null as string,
    email: null as string,
    idade: null as string,
    empresa: null as string
  };
  constructor() { }

  public empty(): EditarModel {
    return <EditarModel>{
      titulo: '',
      modal: false,
      responsive: false,
      width: 0,
      minwidth: 0,
      minY: 0,
      maximizeble: false,
      baseZIndex: 0,
      resizable: false,
      draggable: false,
      closable: false,
      focusOnShow: false,
      visible: false,
      campos: [
        {
          id: 0,
          ordem: 0,
          titulo: '',
          enable: false,
          visible: false,
          campo: '',
          obrigatorio: false,
          tipocampo: '',
          valor: ''
        }
      ]
    };
  }

  public emptyObjTela() {
    return {
      titulo: '',
      id: 0,
      ordem: '',
      enable: false,
      visible: false,
      nome: '',
      email: '',
      idade: '',
      empresa: ''
    };
  }

  set displayModal(show: boolean) {
    this._displayModal = show;
  }
  set displayRota(show: boolean) {
    this._displayRota = show;
  }

  get displayModal() {
    return this._displayModal;
  }
  get displayRota() {
    return this._displayRota;
  }

  set objetosTela(data) {
    this._objetosTela.nome = data.nome;
    this._objetosTela.idade = data.idade;
    this._objetosTela.id = data.id;
    this._objetosTela.email = data.email;
    this._objetosTela.empresa = data.empresa;
  }
  get objetosTela() {
    return this._objetosTela;
  }
}
