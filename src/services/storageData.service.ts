import { UtilService } from './util.service';
import { Injectable } from '@angular/core';

import { GridService } from './grid.service';
import { EditarService } from './editar.service';
import { Spinner } from './../models/spinner.model';
import { Slider } from './../models/slider.model';
import { SelectButton } from './../models/select-button.model';
import { Mask } from './../models/mask.model';
import { Input } from './../models/input.model';
import { GridModel } from './../models/grid.model';
import { EditarModel } from './../models/editar.model';
import { Dropdown } from './../models/dropdown.model';
import { DataSubjectModel } from './../models/dataSubject.model';
import { CrudBtn } from './../models/crud-btn.model';
import { Checkbox } from './../models/checkbox.model';
import { Calendar } from './../models/calendar.model';
import { map } from 'rxjs/operators';

@Injectable()
export class StorageDataService {

  constructor(
    public utilSrv: UtilService
    , public gridSrv: GridService
    , public editarSrv: EditarService) { }

  public getPesquisa() {
    return {
      'title': 'Pesquisa Pessoa',
      'components': [
        {
          'type': 'text',
          'generic': {
            'placeholder': 'Nome da Pessoa',
            'order': 9,
            'value': '',
            'field': 'nomepessoa',
            'visible': true,
            'disable': false
          },
          'specific': {}
        },
        {
          'type': 'text',
          'generic': {
            'placeholder': 'Nome do Aluno',
            'order': 10,
            'value': '',
            'field': 'nomealuno',
            'visible': true,
            'disable': false
          },
          'specific': {}
        },
        {
          'type': 'calendar',
          'generic': {
            'placeholder': 'Data de Nascimento',
            'order': 2,
            'value': '',
            'field': 'datanascimento',
            'visible': true,
            'disable': false
          },
          'specific': {
            'locale': '',
            'dateFormat': 'dd/mm/yy',
            'showIcon': true,
            'minDate': '',
            'maxDate': '',
            'readonlyInput': true,
            'disabledDays': [],
            'yearNavigator': true,
            'monthNavigator': true,
            'yearRange': '',
            'showTime': false,
            'timeOnly': false,
            'selectionMode': '',
            'showButtonBar': true,
            'numberOfMonths': 0,
            'view': '',
            'touchUI': true
          }
        },
        {
          'type': 'autocomplete',
          'generic': {
            'placeholder': 'País',
            'order': 1,
            'value': '',
            'field': 'pais',
            'visible': true,
            'disable': false
          },
          'specific': {
            'options': [
              {
                'nome': 'Brasil'
              },
              {
                'nome': 'Alemanha'
              },
              {
                'nome': 'Portugal'
              }
            ],
            'minLength': 30,
            'multiple': false,
            'dropdown': true
          }
        },
        {
          'type': 'dropdown',
          'generic': {
            'placeholder': 'Cidade',
            'order': 4,
            'value': '',
            'field': 'cidade',
            'visible': true,
            'disable': false
          },
          'specific': {
            'options': [
              {
                'nome': 'Rio de Janeiro',
                'code': 'RJ'
              },
              {
                'nome': 'São Paulo',
                'code': 'SP'
              },
              {
                'nome': 'Minas Gerais',
                'code': 'MG'
              }
            ],
            'optionLabel': '',
            'showClear': true,
            'group': false,
            'filter': false,
            'editable': false
          }
        },
        {
          'type': 'checkbox',
          'generic': {
            'placeholder': 'Times',
            'order': 6,
            'value': '',
            'field': 'times',
            'visible': true,
            'disable': false
          },
          'specific': {
            'options': [
              {
                'groupname': 'times',
                'value': 'Flamengo'
              },
              {
                'groupname': 'times',
                'value': 'Botafogo'
              },
              {
                'groupname': 'times',
                'value': 'Fluminense'
              }
            ],
            'inputId': '',
            'name': '',
            'binary': false
          }
        },
        {
          'type': 'slider',
          'generic': {
            'placeholder': 'Andamento',
            'order': 5,
            'value': '',
            'field': 'andamento',
            'visible': true,
            'disable': false
          },
          'specific': {
            'orientation': '',
            'animate': false,
            'step': 0
          }
        },
        {
          'type': 'spinner',
          'generic': {
            'placeholder': 'Idade',
            'order': 7,
            'value': '',
            'field': 'idade',
            'visible': true,
            'disable': false
          },
          'specific': {
            'min': 0,
            'max': 199,
            'sizer': 0,
            'step': 1
          }
        },
        {
          'type': 'mask',
          'generic': {
            'placeholder': 'CPF',
            'order': 8,
            'value': '',
            'field': 'cpf',
            'visible': true,
            'disable': false
          },
          'specific': {
            'mask': '999.999.999-99',
            'slotChar': ''
          }
        }
      ]
    };
  }
  public CrudButtons(): CrudBtn[] {
    return [{
      titulo: 'INCLUIR',
      id: '10',
      cor: '#486018',
      enable: true,
      visible: false,
      split: false,
      options: null
    }, {
      titulo: 'EDITAR',
      id: '20',
      cor: '#4F8AD8',
      enable: true,
      visible: false,
      split: true,
      options: [{
        label: 'Edição como Rota',
        id: '400',
        command: (event) => {
          if (this.gridSrv.selectedRows.length !== 1 || this.gridSrv.selectedRows.length === undefined) {
            console.log('N tem comp');
          } else {
            this.utilSrv.specificSubjects.rotaEditar.next();
          }
        }
      },
      {
        label: 'Edição como Modal',
        id: '400',
        command: (event) => {
          this.editarSrv.display = false;
          this.utilSrv.specificSubjects.modalEditar.next();
          /* this.utilSrv.ChamarFuncao(null, Config.IDSUBJECT.modalEditar); */
        }
      }]

    }, {
      titulo: 'EXCLUIR',
      id: '30',
      cor: '#D10429',
      enable: true,
      visible: false,
      split: false,
      options: null

    },
    {
      titulo: 'LISTAR',
      id: '40',
      cor: '#636266',
      enable: true,
      visible: false,
      split: false,
      options: null

    },
    {
      titulo: 'EXPORTAR',
      id: '50',
      cor: '#6D8046',
      enable: true,
      visible: false,
      split: false,
      options: null
    }
    ];
  }
}
