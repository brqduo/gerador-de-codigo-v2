import { Injectable } from '@angular/core';
import { Input } from '../models/input.model';
import { Subject } from 'rxjs';

@Injectable()
export class InputService {

  private _Inputs: Array<Input> = [];

  constructor() { }

  get inputList() {
    return this._Inputs;
  }

  set inputList(dados) {
    this._Inputs = dados;
  }

  /**
   * Ativa um botão
   * @param input
   */
  public enable(input: Input) {

  }

  /**
   *
   * Desativa um botão
   * @param {Input} input
   * @memberof InputService
   */
  public disable(input: Input) {

  }



}
