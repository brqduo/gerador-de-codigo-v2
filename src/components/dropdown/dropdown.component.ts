import { Component, OnInit, Input } from '@angular/core';
import { DropdownService } from '../../services/dropdown.service';

@Component({
  selector: 'ons-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  @Input() data = [];
  selectedValue: any;

  constructor(private dropdownSrv: DropdownService) {
  }

  ngOnInit() {

  }
}
