import { Component,Input, OnInit } from '@angular/core';
import { SliderService } from '../../services/slider.service';

@Component({
  selector: 'ons-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @Input() data = [];

  constructor(private sliderSrv: SliderService) { }

  ngOnInit() {
  }

}
