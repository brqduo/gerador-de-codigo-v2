import { Component, OnInit, Input } from '@angular/core';
import { AutoCompleteService } from '../../services/autocomplete.service';

@Component({
  selector: 'ons-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {
  elemento: any;
  elementos: any[];
  filteredElementsSingle: any[];

  @Input() data = [];
  constructor(private autocompleteSrv: AutoCompleteService) { }

  filterElementSingle(event) {

    const query = event.query;
    this.autocompleteSrv.getElementos().subscribe((res: any) => {
      const filtrado = this.filterElement(query, res);
      this.filteredElementsSingle = filtrado;
    });
  }

  filterElement(query, elementos: any): any[] {

    const filtered: any[] = [];
    const suggestions = elementos.specific.options;
    for (let i = 0; i < suggestions.length; i++) {
      const elemento = suggestions[i];

      if (elemento.nome.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(elemento);
      }
    }
    return filtered;
  }

  ngOnInit() {

  }

}
