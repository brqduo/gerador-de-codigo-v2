import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ons-select-button',
  templateUrl: './select-button.component.html',
  styleUrls: ['./select-button.component.scss']
})
export class SelectButtonComponent implements OnInit {
  @Input() data = [];
  selectedType: string;

  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }

}
