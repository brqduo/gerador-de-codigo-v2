import { Component, OnInit, Input } from '@angular/core';
import { CheckboxService } from '../../services/checkbox.service';

@Component({
  selector: 'ons-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [CheckboxService]
})
export class CheckboxComponent implements OnInit {
  @Input() data = [];
  selectedValues: string[] = [];

  constructor(private checkboxSrv: CheckboxService) { }

  ngOnInit() {

  }

}
