
import { DataService } from './../../services/data.service';
import { GridService } from './../../services/grid.service';
import { EditarService } from '../../services/editar.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { CrudButtonService } from '../../services/crud-button.service';
import { UtilService } from '../../services/util.service';
import { DataSubjectModel } from './../../models/dataSubject.model';
import { Config } from '../../environments/config';
import { SelectItem } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'ons-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  gridOptions: GridOptions;
  todoList: any;
  display: boolean;
  gridProprety = {
    enableSort: true,
    enableRowSelection: 'multiple',
    isHorizontalScroll: null as boolean,
    isInlineEditable: null as boolean,
    linesPerPage: null as number,
    limitForPage: null as number,
  };
  Subject = {
    ultimaLinhaSelecionada: new Subject<any>(),
    LinhasSelecionadas: new Subject<any>()
  };
  typeEdit = null as string;
  class = {
    btn1: 'btn btn-secondary',
    btn2: 'btn btn-secondary',
  };
  constructor(
    public http: HttpClient,
    public gridSrv: GridService,
    public crudSrv: CrudButtonService,
    public dataSrv: DataService,
    public editarSrv: EditarService,
    public route: Router,
    public utilSrv: UtilService) {
    const that = this;
    this.gridOptions = {
      rowSelection: this.gridProprety.enableRowSelection,
      enableSorting: this.gridProprety.enableSort,
      rowMultiSelectWithClick: false,
      rowData: null,
      overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Pressione o botão "Ativar grid"</span>',
      columnDefs: null,
      onGridReady: function (params) {
        params.api.sizeColumnsToFit();
      }
    };
    that.inicializarColunas();

    this.utilSrv.FuncaoChamada.subscribe((data: DataSubjectModel) => {
      if (data.idSubject === Config.IDSUBJECT.crudbtn) {
      }
    });
    this.modalStart();
  }


  ngOnInit() {
    this.todoList = [
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];

  }

  showTodo() {
    this.display = true;
  }

  getRows() {
    this.gridSrv.selectedRows = this.gridOptions.api.getSelectedRows();
  }
  inicializarColunas() {
    this.dataSrv.carregarArquivosJson(Config.ARQUIVOSJSON.gridColunas).subscribe((colunas) => {
      this.gridOptions.api.setColumnDefs(colunas);
    });
  }
  iniciarGrid() {
    this.inicializarColunas();
    this.http.get('https://my.api.mockaroo.com/users.json?key=a5e40240').subscribe((res: any) => {
      this.gridOptions.api.setRowData(res);
    });
    /*     this.dataSrv.carregarArquivosJson(Config.ARQUIVOSJSON.gridDados).subscribe((res: any) => {
        }); */
  }

  modalStart() {
    this.gridOptions.onRowDoubleClicked = ((event) => {
      if (this.typeEdit !== null) {
        switch (this.typeEdit) {
          case 'Modal':
            this.editarSrv.displayModal = true;
            this.editarSrv.displayRota = false;
            this.utilSrv.ChamarFuncao(event.data, Config.IDSUBJECT.modalEditar);
            break;
          case 'Rota':
            this.editarSrv.displayRota = true;
            this.editarSrv.displayModal = false;
            this.route.navigateByUrl('Editar');
            this.utilSrv.ChamarFuncao(event.data, Config.IDSUBJECT.rotaEditar);
            break;
        }
      } else {
        console.log('Adicione um tipo de edição');
      }
    });
    this.gridOptions.onRowSelected = ((event) => {
      this.getRows();
    });
  }
  direct(isValid) {
    if (isValid) {
      this.typeEdit = 'Modal';
      this.class.btn1 = 'btn btn-success';
      this.class.btn2 = 'btn btn-secondary';

    } else {
      this.typeEdit = 'Rota';
      this.class.btn2 = 'btn btn-success';
      this.class.btn1 = 'btn btn-secondary';
    }
  }

}
