import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { DataSubjectModel } from './../../models/dataSubject.model';
import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
import { DataService } from '../../services/data.service';
import { EditarService } from '../../services/editar.service';
import { Config } from '../../environments/config';
import { EditarModel } from '../../models/editar.model';
@Component({
  selector: 'ons-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit {
  objetosTela: any = {};

  titulo = '';
  modelTst: any;
  display = true;
  iox: any;
  Arraytest = [{
    fieldName: null as string,
    value: null as any
  }];

  constructor(
    public utilSrv: UtilService
    , public dataSrv: DataService
    , public editarSrv: EditarService
    , public route: Router
  ) { }
  ngOnInit() {
    this.utilSrv.specificSubjects.modalIncluir
      .subscribe((data) => {
        this._montarTela_Modal();
      });
    this.utilSrv.specificSubjects.rotaIncluir
      .subscribe((data) => {
        this._montarTela_Rota();
      });
    this.utilSrv.specificSubjects.modalEditar
      .subscribe((data) => {
        this._montarTela_Modal(data);
      });
    this.utilSrv.specificSubjects.rotaEditar
      .asObservable().subscribe((data) => {
        this._montarTela_Rota(data);
      });
  }
  private _montarTela_Modal(data?) {
    if (data === undefined) {
      this.editarSrv.displayModal = true;
      this.editarSrv.displayRota = false;
      this.editarSrv.objetosTela = this.editarSrv.emptyObjTela();
      this.editarSrv.objetosTela.titulo = 'Incluir';
    } else {
      this.editarSrv.displayModal = true;
      this.editarSrv.displayRota = false;
      this.editarSrv.objetosTela = data;
      this.editarSrv.objetosTela.titulo = 'Editar';
    }
  }
  private _montarTela_Rota(data?) {
    if (data === undefined) {
      this.editarSrv.displayRota = true;
      this.editarSrv.displayModal = false;
      this.editarSrv.objetosTela = this.editarSrv.emptyObjTela();
      this.editarSrv.objetosTela.titulo = 'Incluir';
    } else {
      this.editarSrv.displayRota = true;
      this.editarSrv.displayModal = false;
      this.editarSrv.objetosTela = data;
      this.editarSrv.objetosTela.titulo = 'Editar';
    }
  }







  voltar() {
    this.editarSrv.displayModal = false;
    this.editarSrv.displayRota = false;
    this.route.navigateByUrl('Home');
  }
  salvar() {
    const retorno = [];
    console.clear();
    this.modelTst.id = 100;
    const property = Object.keys(this.modelTst);
    const values = Object.values(this.modelTst);
    for (let i = 0; i < property.length; i++) {
      retorno.push({
        campo: property[i],
        valor: values[i]
      });
    }
    console.log(retorno);
  }

}
