import { Component, Input, OnInit} from '@angular/core';
import { CalendarService } from '../../services/calendar.service';

@Component({
  selector: 'ons-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  @Input() data = [];

  constructor(private calendarSrv: CalendarService) { }

  ngOnInit() {

  }
}
