import { Component, OnInit, Input } from '@angular/core';
import { MaskService } from '../../services/mask.service';

@Component({
  selector: 'ons-mask',
  templateUrl: './mask.component.html',
  styleUrls: ['./mask.component.scss']
})
export class MaskComponent implements OnInit {
  @Input() data = [];

  constructor(private maskSrv: MaskService) { }

  ngOnInit() {

  }
}
