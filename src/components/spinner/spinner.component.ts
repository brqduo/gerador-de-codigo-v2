import { Component,Input, OnInit } from '@angular/core';
import { SpinnerService } from '../../services/spinner.service';

@Component({
  selector: 'ons-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input() data = [];

  constructor(private spinnerSrv: SpinnerService ) { }

  ngOnInit() {

  }

}
