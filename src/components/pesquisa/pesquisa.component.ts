import {
  Component,
  OnInit,
  ViewChild,
  ComponentFactoryResolver,
  ViewContainerRef,
  ElementRef,
  Renderer2
} from '@angular/core';

// services
import { InputService } from '../../services/input.service';
import { UtilService } from '../../services/util.service';
import { PesquisaService } from '../../services/pesquisa.service';
import { CalendarService } from '../../services/calendar.service';
import { SpinnerService } from '../../services/spinner.service';

// componentes
import { InputComponent } from '../../components/input/input.component';
import { CalendarComponent } from '../calendar/calendar.component';
import { SpinnerComponent } from '../spinner/spinner.component';
import { SliderComponent } from '../slider/slider.component';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { MaskComponent } from '../mask/mask.component';
import { AutocompleteComponent } from '../autocomplete/autocomplete.component';

import { AutoCompleteService } from '../../services/autocomplete.service';
import { CheckboxService } from '../../services/checkbox.service';
import { DropdownService } from '../../services/dropdown.service';
import { MaskService } from '../../services/mask.service';
import { SliderService } from '../../services/slider.service';
import { DataService } from '../../services/data.service';
import { Config } from '../../environments/config';
import { SelectButtonService } from '../../services/select-button.service';
import { SelectButtonComponent } from '../select-button/select-button.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'ons-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.scss']
})
export class PesquisaComponent implements OnInit {
  private _tab = true;
  public inputPesquisa = '';
  public pesquisaObj = [];
  componentRef: any;
  private columnsNumber = 0;
  private lastGrid: any;
  private FiltroSelect: any = [];

  @ViewChild('componentes', { read: ViewContainerRef }) entry: ViewContainerRef;
  @ViewChild('componentes') myDiv: ElementRef;
  @ViewChild('filtros', { read: ViewContainerRef }) entryFilter: ViewContainerRef;
  @ViewChild('filtros') myDivFilter: ElementRef;
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private renderer: Renderer2,
    private utilSrv: UtilService,
    private pesquisaSrv: PesquisaService,
    private inputSrv: InputService,
    private calendarSrv: CalendarService,
    private spinnerSrv: SpinnerService,
    private autocompleteSrv: AutoCompleteService,
    private checkboxSrv: CheckboxService,
    private dropdownSrv: DropdownService,
    private maskSrv: MaskService,
    private sliderSrv: SliderService,
    private selectButtonSrv: SelectButtonService,
    private dataSrv: DataService
  ) { }

  ngOnInit() {
    this.dataSrv.carregarArquivosJson(Config.ARQUIVOSJSON.pesquisa)
      .subscribe((res: any) => {
        this.pesquisaSrv.camposPesquisa = res.components;
        this.pesquisaSrv.title = res.yitle;
        this.pesquisaSrv.idPesquisa = res.id;
        this.pesquisaSrv.camposPesquisa.sort((obj1: any, obj2: any) => obj1.generic.order - obj2.generic.order);
        this.lerObjetoComponente();
      });

      this.dataSrv.carregarFiltros(Config.ARQUIVOSJSON.columns)
      .subscribe((res: any) => {
          this.FiltroSelect = res;
          this.lerObjetoFiltro();
      });

  }

  /**
   * Verifica a abertura do accordion para mostrar ou não o input de pesquisa simples
   * @param isOpen
   * @param elemento
   */
  private onTab(isOpen, elemento) {
    this._tab = isOpen;
  }

  /**
   * Verifica o tipo de componente para construção
   * @param objeto
   */
  lerObjetoComponente() {
    if (this.pesquisaSrv.camposPesquisa) {
      this.pesquisaSrv.camposPesquisa.forEach(element => {
        if (element.type === 'text') {
          this.inputSrv.inputList = element;
          const inputComponentElement = this.construirComponente(element, InputComponent);
          this.construirFlex(this.myDiv.nativeElement, inputComponentElement);
        } else if (element.type === 'number' || element.type === 'spinner') {
          this.spinnerSrv.spinnerList = element;
          const spinnerComponentElement = this.construirComponente(element, SpinnerComponent);
          this.construirFlex(this.myDiv.nativeElement, spinnerComponentElement);
        } else if (element.type === 'calendar') {
          this.calendarSrv.calendarList = element;
          const calendarComponentElement = this.construirComponente(element, CalendarComponent);
          this.construirFlex(this.myDiv.nativeElement, calendarComponentElement);
        } else if (element.type === 'slider') {
          this.sliderSrv.sliderList = element;
          const sliderComponentElement = this.construirComponente(element, SliderComponent);
          this.construirFlex(this.myDiv.nativeElement, sliderComponentElement);
        } else if (element.type === 'autocomplete') {
          this.autocompleteSrv.autoCompleteList = element;
          const sliderComponentElement = this.construirComponente(element, AutocompleteComponent);
          this.construirFlex(this.myDiv.nativeElement, sliderComponentElement);
        } else if (element.type === 'checkbox') {
          this.checkboxSrv.checkboxList = element;
          const checkboxComponentElement = this.construirComponente(element, CheckboxComponent);
          this.construirFlex(this.myDiv.nativeElement, checkboxComponentElement);
        } else if (element.type === 'dropdown') {
          this.dropdownSrv.dropdownList = element;
          const dropdownComponentElement = this.construirComponente(element, DropdownComponent);
          this.construirFlex(this.myDiv.nativeElement, dropdownComponentElement);
        } else if (element.type === 'mask') {
          this.maskSrv.maskList = element;
          const maskComponentElement = this.construirComponente(element, MaskComponent);
          this.construirFlex(this.myDiv.nativeElement, maskComponentElement);
        } else {
          this.utilSrv.alerta('O componente do tipo ' + element.type + ' não existe para construção.', 'Erro de componente', Config.TIPOMENSAGEM.error);
        }
      });
    }

  }

  lerObjetoFiltro() {
    if (this.FiltroSelect.specific.options.length > 0) {
          // DROPDOWN
          const dropdownComponentElement = this.construirComponente(this.FiltroSelect, DropdownComponent);

          const grid = this.renderer.createElement('div');
          this.renderer.addClass(grid, 'p-grid');

          const div = this.renderer.createElement('div');
          this.renderer.addClass(div, 'p-col-2');

          const box = this.renderer.createElement('div');
          this.renderer.addClass(box, 'box');

          this.renderer.appendChild(box, dropdownComponentElement);
          this.renderer.appendChild(div, box);
          this.renderer.appendChild(grid, div);
          this.renderer.appendChild(this.myDivFilter.nativeElement, grid);


          // ASCENDENTE OU DESCENDENTE

          this.selectButtonSrv.dropdownList = Config.PESQUISA_AVANCADA.filtro_ordenacao;
          const selectButtonComponentElement = this.construirComponente(this.selectButtonSrv.dropdownList, SelectButtonComponent);

          const divSelect = this.renderer.createElement('div');
          this.renderer.addClass(divSelect, 'p-col-3');

          const boxSelect = this.renderer.createElement('div');
          this.renderer.addClass(boxSelect, 'box');

          this.renderer.appendChild(boxSelect, selectButtonComponentElement);
          this.renderer.appendChild(divSelect, boxSelect);
          this.renderer.appendChild(grid, divSelect);
          this.renderer.appendChild(this.myDivFilter.nativeElement, grid);

    }
  }


  /**
   * Constrói o html do componente responsivo para renderizar na tela
   * @param componente
   */
  private construirFlex(DivSuperior, componente) {

    const myDiv: HTMLElement = DivSuperior;

    const grid = this.renderer.createElement('div');
    this.renderer.addClass(grid, 'p-grid');

    const div = this.renderer.createElement('div');
    this.renderer.addClass(div, 'p-col');

    const box = this.renderer.createElement('div');
    this.renderer.addClass(box, 'box');

    this.renderer.appendChild(box, componente);
    this.renderer.appendChild(div, box);

    if (this.columnsNumber === 0) {
      this.renderer.appendChild(grid, div);
      this.renderer.appendChild(myDiv, grid);
      this.lastGrid = grid;
      this.columnsNumber = this.columnsNumber + 1;
    } else {
      this.columnsNumber = this.columnsNumber + 1;
      if ( this.columnsNumber > Config.PESQUISA_AVANCADA.maxColumns) {
        this.columnsNumber = 0;
      } else {
        this.renderer.appendChild(this.lastGrid, div);
        this.renderer.appendChild(myDiv, this.lastGrid);
      }
    }

  }

  /**
   * Monta objeto para enviar para pesquisa de dados
   */
  private cliquePesquisar() {
    const arrayAux = [];
    let idPesquisa = Config.IDSUBJECT.pesquisa;
    if (this._tab) {
      if (this.inputPesquisa !== '') {
        this.utilSrv.ChamarFuncao(this.inputPesquisa, Config.IDSUBJECT.pesquisa);
      } else {
        this.utilSrv.alerta('Campo pesquisa não pode ser vazio.', 'Pesquisa Vazia', Config.TIPOMENSAGEM.error);
      }
    } else {
      this.pesquisaSrv.camposPesquisa.forEach(element => {
        if (element.generic.value !== '') {
          arrayAux.push({ 'field': element.generic.field, 'value': element.generic.value });
        }
      });

      idPesquisa = this.pesquisaSrv.idPesquisa;
    }
    this.utilSrv.ChamarFuncao(arrayAux, idPesquisa);
  }

  /**
   *
   * Constrói a instância do componente que será inserida na tela
   *
   * @param Data Dados ( Propriedades do Componente)
   * @param Componente Componente em Si
   */
  construirComponente(Data, Componente) {
    const factory = this.componentFactoryResolver.resolveComponentFactory(Componente);
    this.componentRef = this.entry.createComponent(factory);
    this.componentRef.instance.data = Data;
    return this.componentRef.location.nativeElement;
  }

  destroyComponent() {
    this.componentRef.destroy();
  }

}
