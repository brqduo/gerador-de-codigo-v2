import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudbuttonsComponent } from './crudbuttons.component';

describe('CrudbuttonsComponent', () => {
  let component: CrudbuttonsComponent;
  let fixture: ComponentFixture<CrudbuttonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudbuttonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudbuttonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
