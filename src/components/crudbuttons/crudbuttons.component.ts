import { Router } from '@angular/router';
import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { CrudBtn } from '../../models/crud-btn.model';
import { CrudButtonService } from '../../services/crud-button.service';
import { Config } from '../../environments/config';
import { UtilService } from '../../services/util.service';
import { DataSubjectModel } from './../../models/dataSubject.model';

@Component({
  selector: 'ons-crudbuttons',
  templateUrl: './crudbuttons.component.html',
  styleUrls: ['./crudbuttons.component.scss']
})
export class CrudbuttonsComponent implements OnInit {
  items: MenuItem[];
  CrudButtons: Array<CrudBtn>;
  selectedBtn: any;
  stylebtn: any;
  constructor(
    public crudButtonSrv: CrudButtonService,
    public dataSrv: DataService,
    public route: Router,
    public utilSrv: UtilService) {

    this.utilSrv.FuncaoChamada.subscribe((data: DataSubjectModel) => {
      if (data.idSubject === Config.IDSUBJECT.crudbtn) {
        this.log(data);
      }
    });


  }
  ngOnInit() {
    this.stylebtn = {
      'color': 'red',
      'background-color': '#486018'
    };
    this.carregar();
  }

  carregar() {
    this.CrudButtons = this.crudButtonSrv.buttonList;
    /*     this.dataSrv.carregarArquivosJson(Config.ARQUIVOSJSON.botoes).subscribe((res) => {
      this.CrudButtons = res;
    }); */
  }
  getClick(item) {
    console.log('Qual foi o btn pressionado =>', item);
    this.selectedBtn = item;
  switch (item.id) {
      case Config.IDSUBJECT.modalIncluir:
        this.utilSrv.ChamarFuncao('', item.id);
        this.utilSrv.ChamarFuncao(item, Config.IDSUBJECT.crudbtn);
        break;
      case Config.IDSUBJECT.rotaIncluir:
        this.utilSrv.ChamarFuncao('', item.id);
        this.route.navigateByUrl('Editar');
        this.utilSrv.ChamarFuncao(item, Config.IDSUBJECT.crudbtn);
        break;
    }
  }

  colortest() {

    const onsColors: string[] = Config.onsColorPallete;
    const color = onsColors[Math.floor(Math.random() * onsColors.length)];
    return color;
  }

  log(event) {
    console.log('Log solicitado', event);
  }



}
