import { Component, Input, OnInit } from '@angular/core';
import { InputService } from '../../services/input.service';

@Component({
  selector: 'ons-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  @Input() data = [];

  constructor(
    private inputSrv?: InputService
  ) { }

  ngOnInit() {

  }
}
