import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ons-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  title = 'gerador-ons';
  page = true;
  class = {
    btn1: 'btn btn-secondary',
    btn2: 'btn btn-secondary',
  };
  data = [
    {
      type: 'text',
      Placeholder: 'Nome'
    },
    {
      type: 'text',
      Placeholder: 'Idade'
    },
    {
      type: 'text',
      Placeholder: 'Commit'
    }
  ];

  classbtn1 = 'btn btn-secondary';
  classbtn2 = 'btn btn-secondary';
  constructor() { }

  ngOnInit() {
  }


  direct(isValid) {
    if (isValid) {
      this.page = false;
      this.class.btn1 = 'btn btn-success';
      this.class.btn2 = 'btn btn-secondary';
      console.log('true', this.classbtn1);

    } else {
      this.page = true;
      this.class.btn2 = 'btn btn-success';
      this.class.btn1 = 'btn btn-secondary';
      console.log('false', this.class);
    }
  }

  retornaElemento() {

  }


}
