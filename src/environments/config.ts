export const Config = {
  onsColorPallete: [
    // #verde
    '#486018',
    '#6D8046',
    '#91A074',
    // #cinza
    '#636266',
    '#828185',
    '#A1A1A3',
    // #Amarelo
    '#FBC90B',
    '#FCD43C',
    '#FDDF6D',
    // #Laranja
    '#F76C00',
    '#F98933',
    '#FAA766',
    // #Vermelho
    '#D10429',
    '#DA3654',
    '#E3687F',
    // #Azul
    '#4F8AD8',
    '#72A1E0',
    '#95B9E8',
  ],
  TIPOMENSAGEM: {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
  },
  CRUD_BTN_IDs: {
    incluir: '10',
    editar: '20',
    excluir: '30',
    listar: '40',
    exportar: '50',
  },
  CAMINHODADOS: {
    url_dsv: '../assets/data/',
    url_hmg: '../assets/data/',
    url_prd: '../assets/data/'
  },
  CAMINHO_COLUMNS_DEF: {
    url_dsv: '../assets/data/',
    url_hmg: '../assets/data/',
    url_prd: '../assets/data/'
  },
  AMBIENTES: {
    dsv: 'dsv',
    hmg: 'hmg',
    prd: 'prd'
  },
  ARQUIVOSJSON: {
    pesquisa: 'pesquisa.json',
    botoes: 'botoes.json',
    gridColunas: 'gridColumns.json',
    gridDados: 'gridData.json',
    columns: 'columns.json',
    campostemplate: 'campostemplate.json'
  },
  IDSUBJECT: {
    pesquisa: 'pesquisa',
    grid: 'grid',
    crudbtn: 'crudbtn',
    modalEditar: 'modalEditar',
    modalIncluir: 'modalIncluir',
    rotaEditar: 'rotaEditar',
    rotaIncluir: 'rotaIncluir'
  },
  PESQUISA_AVANCADA: {
    maxColumns: 4,
    filtro_ordenacao: [
      { label: 'Ascendente', value: 0 },
      { label: 'Descendente', value: 1 }
    ]
  },
  CLASSES_PADRONIZADAS: {
    BtnVoltar: 'BtnVoltar',
    BtnIncluir: 'BtnIncluir',
    BtnEditar: 'BtnEditar',
    BtnExcluir: 'BtnExcluir',
    BtnListar: 'BtnListar',
    BtnExportar: 'BtnExportar',
  }

};
