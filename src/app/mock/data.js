export const data = {
  Title: 'Pesquisa Pessoa',
  Components: [
    {
      type: "text",
      Generic: {
        placeholder: 'Nome',
        mask: '',
        order: 11,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "email",
      Generic: {
        Placeholder: 'E-mail',
        mask: '',
        order: 2,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "calendar",
      Generic: {
        Placeholder: 'Data Nascimento',
        mask: '',
        order: 3,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "auto-complete",
      Generic: {
        Placeholder: 'Data Nascimento',
        mask: '',
        order: 4,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "dropdown",
      Generic: {
        Placeholder: 'Data Nascimento',
        mask: '',
        order: 5,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "checkbox",
      Generic: {
        Placeholder: 'Data Nascimento',
        mask: '',
        order: 6,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      },
      checkbox: [{
        groupname: "times",
        value: "Flamengo"
      }, {
        groupname: "times",
        value: "Botafogo"
      }, {
        groupname: "times",
        value: "Fluminense"
      }
      ]
    },
    {
      type: "slider",
      Generic: {
        Placeholder: 'Dias Corridos',
        mask: '',
        order: 7,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "spinner",
      Generic: {
        Placeholder: 'Data Nascimento',
        mask: '',
        order: 8,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "mask",
      Generic: {
        Placeholder: 'Máscara',
        mask: '999.999.999-99',
        order: 9,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    },
    {
      type: "number",
      Generic: {
        Placeholder: 'Quantidade',
        //mask:'999.999.999-99',
        order: 10,
        visible: true,
        enabled: true
      },
      AutoComplete: {

      },
      KeyFilter: {

      }
    }
  ]
};

