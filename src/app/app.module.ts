

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { AgGridModule } from 'ag-grid-angular';

// Componentes
import { PesquisaComponent } from '../components/pesquisa/pesquisa.component';
import { GridComponent } from '../components/grid/grid.component';
import { CrudbuttonsComponent } from '../components/crudbuttons/crudbuttons.component';
import { TestesComponent } from '../components/testes/testes.component';
import { InputComponent } from '../components/input/input.component';
import { CalendarComponent } from '../components/calendar/calendar.component';
import { DropdownComponent } from '../components/dropdown/dropdown.component';
import { CheckboxComponent } from '../components/checkbox/checkbox.component';
import { SliderComponent } from '../components/slider/slider.component';
import { SpinnerComponent } from '../components/spinner/spinner.component';
import { AutocompleteComponent } from '../components/autocomplete/autocomplete.component';
import { MaskComponent } from '../components/mask/mask.component';
import { SelectButtonComponent } from '../components/select-button/select-button.component';
import { EditarComponent } from '../components/editar/editar.component';

// Serviços
import { DataService } from '../services/data.service';
/* import { CallFunctionService } from '../services/callFunction.service'; */
import { CrudButtonService } from '../services/crud-button.service';
import { UtilService } from '../services/util.service';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { PesquisaService } from '../services/pesquisa.service';
import { CalendarService } from '../services/calendar.service';
import { SpinnerService } from '../services/spinner.service';
import { CheckboxService } from '../services/checkbox.service';
import { DropdownService } from '../services/dropdown.service';
import { MaskService } from '../services/mask.service';
import { SliderService } from '../services/slider.service';
import { AutoCompleteService } from '../services/autocomplete.service';
import { RouterModule, Routes } from '@angular/router';
import { InputService } from '../services/input.service';
import { GridService } from '../services/grid.service';
import { SelectButtonService } from '../services/select-button.service';
import { StorageDataService } from '../services/storageData.service';


// Modules
import { PrimeNGModule } from '../module/prime-ng.module';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DialogModule } from 'primeng/dialog';
import { HomePageComponent } from '../pages/homePage/home-page.component';





const routes: Routes = [
  { path: 'Home', component: HomePageComponent },
  { path: 'Editar', component: EditarComponent },
  { path: '', redirectTo: 'Home', pathMatch: 'full' },

];


@NgModule({
  declarations: [
    AppComponent,
    // paginas
    HomePageComponent,
    EditarComponent,
    // Componentes
    PesquisaComponent,
    GridComponent,
    CrudbuttonsComponent,
    InputComponent,
    CalendarComponent,
    TestesComponent,
    DropdownComponent,
    CheckboxComponent,
    SliderComponent,
    SpinnerComponent,
    AutocompleteComponent,
    MaskComponent,
    SelectButtonComponent,
  ],
  entryComponents: [
    InputComponent,
    SpinnerComponent,
    CalendarComponent,
    SliderComponent,
    MaskComponent,
    GridComponent,
    EditarComponent,
    DropdownComponent,
    CheckboxComponent,
    AutocompleteComponent,
    SelectButtonComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PrimeNGModule,
    HttpClientModule,
    InputTextModule,
    SelectButtonModule,
    DialogModule,
    RouterModule.forRoot(routes),
    AgGridModule.withComponents([/* rowBottomStyleComponent */]),
  ],
  providers: [
    DataService,
    /* CallFunctionService, */
    CrudButtonService,
    GridService,
    UtilService,
    MessageService,
    ConfirmationService,
    PesquisaService,
    CalendarService,
    SpinnerService,
    CheckboxService,
    DropdownService,
    MaskService,
    InputService,
    SliderService,
    AutoCompleteService,
    SelectButtonService,
    StorageDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
