import { Component } from '@angular/core';

@Component({
  selector: 'ons-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'gerador-ons';
  page = true;
  class = {
    btn1: 'btn btn-secondary',
    btn2: 'btn btn-secondary',
  };
  data = [
    {
      type:'text',
      Placeholder: 'Nome'
    },
    {
      type:'text',
      Placeholder: 'Idade'
    },
    {
      type:'text',
      Placeholder: 'Commit'
    }
  ]

  classbtn1 = 'btn btn-secondary';
  classbtn2 = 'btn btn-secondary';
  constructor() { }


direct(isValid) {
  if (isValid) {
    this.page = false;
    this.class.btn1 = 'btn btn-success';
    this.class.btn2 = 'btn btn-secondary';
    console.log('true', this.classbtn1);

  } else {
    this.page = true;
    this.class.btn2 = 'btn btn-success';
    this.class.btn1 = 'btn btn-secondary';
    console.log('false', this.class);
  }
}

retornaElemento() {

}




}
