export interface Slider {
    type: number;
    Generic: {
      Placeholder: string;
      order: number;
      visible: boolean;
      enabled: true;
      size: number;
      styleClass: string;
      label: string;
    };
    Specific: {
      orientation: string;
      animate: boolean;
      step: number;
    };
}
