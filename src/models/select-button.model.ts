export interface SelectButton {
    type: string;
    Generic: {
        Placeholder: string;
        order: number;
        visible: boolean;
        enabled: true;
        size: number;
        styleClass: string;
        label: string;
        };
        Specific: {
            options: any[];
            optionLabel: string;
            showClear: boolean;
            group: boolean;
            filter: boolean;
            editable: boolean;
        };
    }
