export interface Spinner {
    type: number;
    Generic: {
      Placeholder: string;
      order: number;
      visible: boolean;
      enabled: true;
      size: number;
      styleClass: string;
      label: string;
    };
    Specific: {
      min: number;
      max: number;
      sizer: number;
      step: number;
    };
}
