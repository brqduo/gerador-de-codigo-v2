export interface Input {
        type: string;
        Generic: {
          placeholder: string;
          order: number;
          visible: boolean;
          enabled: true;
          size: number;
          styleClass: string;
          label: string;
        };
}
