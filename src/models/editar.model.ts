export interface EditarModel {
    titulo: string;
    modal: boolean;
    responsive: boolean;
    width: number;
    minwidth: number;
    minY: number;
    maximizeble: boolean;
    baseZIndex: number;
    resizable: boolean;
    draggable: boolean;
    closable: boolean;
    focusOnShow: boolean;
    visible: boolean;
    campos: [
        {
            id: number;
            ordem: number;
            titulo: string;
            enable: boolean;
            visible: boolean;
            campo: string;
            obrigatorio: boolean;
            tipocampo: string;
            valor: string;
        }
    ];
}
