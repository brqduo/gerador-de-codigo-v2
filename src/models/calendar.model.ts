export interface Calendar {
  type: string;
  Generic: {
    Placeholder: string;
    order: number;
    visible: boolean;
    enabled: true;
    size: number;
    styleClass: string;
    label: string;
  };
  Specific: {
    locale: string;
    dateFormat: string;
    showIcon: boolean;
    minDate: Date;
    maxDate: Date;
    readonlyInput: boolean;
    disabledDays: number[];
    yearNavigator: boolean;
    monthNavigator: boolean;
    yearRange: string;
    showTime: boolean;
    timeOnly: boolean;
    selectionMode: string;
    showButtonBar: boolean;
    numberOfMonths: number;
    view: string;
    touchUI: boolean;
  };
}
