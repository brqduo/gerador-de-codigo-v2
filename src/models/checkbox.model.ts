export interface Checkbox {
type: string;
Generic: {
    Placeholder: string;
    order: number;
    visible: boolean;
    enabled: true;
    size: number;
    styleClass: string;
    label: string;
    };
    Specific: {
        inputId: string;
        value: string;
        name: string;
        binary: boolean;
    };
}
