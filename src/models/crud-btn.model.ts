import { MenuItem } from 'primeng/api';
export interface CrudBtn {
    titulo: string;
    id: string;
    cor: string;
    enable: boolean;
    visible: boolean;
    split: boolean;
    options: any[];
}
