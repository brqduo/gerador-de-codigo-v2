export interface AutoComplete {
    type: string;
    Generic: {
        Placeholder: string;
        order: number;
        visible: boolean;
        enabled: true;
        size: number;
        styleClass: string;
        label: string;
    };
    Specific: {
        sugestoes: any[];
        minLength: number;
        multiple: boolean;
        dropdown: boolean;
    };
}
