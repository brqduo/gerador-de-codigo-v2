export interface GridModel {
    headerName: string;
    field: string;
    width: number;
    columnGroupShow?: string;
    checkboxSelection: boolean;
}
