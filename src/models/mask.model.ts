export interface Mask {
  type: string;
  Generic: {
    Placeholder: string;
    order: number;
    visible: boolean;
    enabled: true;
    size: number;
    styleClass: string;
    label: string;
  };
  Specific: {
    mask: string;
    slotChar: string;
  };
}
